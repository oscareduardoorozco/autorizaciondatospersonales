
export class Norma {

  pCodigo: string | undefined;
  pDescripcion: string | undefined;
  pFechaInicio: Date | undefined;
  pFechaFin: Date | undefined;

}
