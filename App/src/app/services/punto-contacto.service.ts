import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PuntoContactoService {
 private _url='http://localhost:65167/api/autdatos/punto_contacto/getpuntoscontacto';
  //private _url='https://APIAutDatosDes.chec.com.co/api/autdatos/punto_contacto/getpuntoscontacto';
 //private _url2='https://APIAutDatosDes.chec.com.co/api/autdatos/punto_contacto/registro_punto_contacto';
  private _url2='http://localhost:65167/api/autdatos/punto_contacto/registro_punto_contacto';
  private _url3='http://localhost:65167/api/autdatos/punto_contacto/update_puntocontacto';
  //private _url3='https://APIAutDatosDes.chec.com.co/api/autdatos/punto_contacto/update_puntocontacto';
  private _url4='http://localhost:65167/api/autdatos/punto_contacto/delete_puntocontacto_x_nombre';
  //private _url4='https://APIAutDatosDes.chec.com.co/api/autdatos/punto_contacto/delete_puntocontacto_x_nombre';


  constructor(private http: HttpClient) { }

  getListarpuntocontacto(): Observable<any >{
    return this.http.get(this._url);
   }

   agregarContacto(contacto: any){
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    console.log('objeto contacto',contacto);

    var obj = {pNombre:contacto.pNombre,pDescripcion:contacto.pDescripcion, pCanal:contacto.pCanal, pActivo:contacto.pActivo}
    console.log('objeto resultado',obj);
    return this.http.post(this._url2, obj);


}

  // modales punto de contacto modificar y eliminar
modificarContacto(modificarContacto: any){
  console.log("modificarContacto",modificarContacto)
  debugger
 var obj={ pNombre:modificarContacto.Nombre,
           pDescripcion:modificarContacto.Descripcion,
           pCanal:modificarContacto.Canal,
           pActivo:modificarContacto.Activo}
           console.log("obj",obj)
 return this.http.post(this._url3, obj);
}

eliminarContacto(contacto: any): Observable<any >{
  console.log("aqui llego a eliminar contacto",contacto)
  var obj={pNombre:contacto}
  return this.http.post(this._url4,obj);
}

}
