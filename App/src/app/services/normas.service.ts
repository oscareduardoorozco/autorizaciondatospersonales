import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NormasService {
  [x: string]: any;
  private _url='http://localhost:65167/api/autdatos/norma/getnormas';
  // private _url='https://APIAutDatosDes.chec.com.co/api/autdatos/norma/getnormas';
 //private _url2='https://APIAutDatosDes.chec.com.co/api/autdatos/norma/registro_norma';
  private _url2='http://localhost:65167/api/autdatos/norma/registro_norma';
  private _url3='http://localhost:65167/api/autdatos/norma/update_norma';
  private _url4='http://localhost:65167/api/autdatos/norma/delete_norma_x_codigo';
  private _url5='http://localhost:65167/api/autdatos/norma/getnormasactivas';


  constructor(private http: HttpClient) {}
   getListarnormas(): Observable<any >{
    return this.http.get(this._url);
   }

   getListarnormasactivas(): Observable<any >{
    return this.http.get(this._url5);
   }
  agregarNorma(norma: any){
      let headers = new HttpHeaders().set('Content-Type', 'application/json');
      console.log('objeto norma',norma);

      var fechaInicio = new Date(norma.pFechaInicio.year, norma.pFechaInicio.month-1, norma.pFechaInicio.day);
      var fechaFin = new Date(norma.pFechaFin.year, norma.pFechaFin.month-1, norma.pFechaFin.day);

      var obj = {pCodigo:norma.pCodigo, pDescripcion:norma.pDescripcion, pFechaInicio:fechaInicio, pFechaFin:fechaFin}
      console.log('objeto resultado',obj);
      return this.http.post(this._url2, obj);


  }

  modificarNorma(modificarNorma: any){
    console.log("modificarNorma",modificarNorma)
    debugger
   var obj={ pCodigo:modificarNorma.Codigo,
             pDescripcion:modificarNorma.Descripcion,
             pFechaFin:modificarNorma.pFechaFin}
             console.log("obj",obj)
   return this.http.post(this._url3, obj);
  }

  eliminarNorma(CodigoNorma: any): Observable<any >{
    console.log("aqui llego a eliminar contacto",CodigoNorma)
    var obj={pCodigo:CodigoNorma}
    return this.http.post(this._url4,obj);
  }
}

