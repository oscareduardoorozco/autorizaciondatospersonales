import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {  Observable} from 'rxjs';
import { LoginI} from '../../app/models/login.interface';
import { ResponseI} from '../../app/models/response.interface';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  //private _url='https://APIAutDatosDes.chec.com.co/api/autdatos/usuario';
  //private _url2='https://APIAutDatosDes.chec.com.co/api/autdatos/usuario/authenticate';
  private _url2='http://localhost:65167/api/autdatos/usuario/authenticate';

  constructor(private http: HttpClient) { }

  loginByUsername(form: LoginI):Observable<any> {

    return this.http.post<any>(this._url2,form);



  }

}
