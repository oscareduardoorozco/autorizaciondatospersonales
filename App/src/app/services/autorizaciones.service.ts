import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutorizacionesService {
   //private _url=' http://localhost:65167/api/autdatos/norma/getnormas';
   //private _url='https://APIAutDatosDes.chec.com.co/api/autdatos/autorizacion/autorizacionxid/{id}';
   //private _url2='https://APIAutDatosDes.chec.com.co/api/autdatos/autorizacion/registro_autorizacion';
  private _url2='http://localhost:65167/api/autdatos/autorizacion/registro_autorizacion';
  // private _url3='https://APIAutDatosDes.chec.com.co/api/autdatos/contacto/get_contacto_x_id';
   //private _url4='https://APIAutDatosDes.chec.com.co/api/autdatos/contacto/registro_contacto';
   private _url='https://APIAutDatosDes.chec.com.co/api/autdatos/autorizacion/autorizaciones';
   //private _url6='https://APIAutDatosDes.chec.com.co/api/autdatos/autorizacion/autorizacionxid';
   //private _url5='https://APIAutDatosDes.chec.com.co/api/autdatos/contacto/delete_contact_x_id';
   //private _url7='https://APIAutDatosDes.chec.com.co/api/autdatos/contacto/update_contact';
   private _url3='http://localhost:65167/api/autdatos/contacto/get_contacto_x_id';
   private _url4='http://localhost:65167/api/autdatos/contacto/registro_contacto';
  //private _url=' http://localhost:65167/api/autdatos/autorizacion/autorizaciones';
  private _url6=' http://localhost:65167/api/autdatos/autorizacion/autorizacionxid';
  private _url5=' http://localhost:65167/api/autdatos/contacto/delete_contact_x_id';
  private _url7=' http://localhost:65167/api/autdatos/contacto/update_contact';




  constructor(private http: HttpClient) { }

  getListarautorizacion(): Observable<any >{
    return this.http.get(this._url);
   }

   getListarcontactoautorizacion(idContacto: any): Observable<any >{
    return this.http.get(this._url3+'/'+idContacto.pPersonaId);
   }
   getListarautorizacionxId(idContacto: any): Observable<any >{

    return this.http.get(this._url6+'/'+idContacto.pPersonaId);

   }
   modificarAutoContacto(modificarContacto: any){
     console.log("modificarContacto",modificarContacto)
     debugger
    var obj={ pId:modificarContacto.Id,
              pActivo:modificarContacto.Activo,
              pContacto:modificarContacto.Contacto,
              pTipoContactoId:modificarContacto.TipoContactoId}
              console.log("obj",obj)
    return this.http.post(this._url7, obj);
   }
agregarautoContacto(autcontacto: any,idPersona: any){
    //let headers = new HttpHeaders().set('Content-Type', 'application/json');
    console.log('objeto agregar contacto',autcontacto,idPersona);

    var obj = {pTipoContactoId:autcontacto.pTipoContactoId,pContacto:autcontacto.pContacto,pActivo:autcontacto.pActivo,pPersonaId:idPersona}
    console.log('objeto a enviar',obj);

    return this.http.post(this._url4, obj);

}

agregarautorizacion(autorizacion: any){
  let headers = new HttpHeaders().set('Content-Type', 'application/json');
  console.log('objeto norma',autorizacion);

  var obj = { pPersonaid:autorizacion.pNumero,
              pNombrePersona:autorizacion.pNombrePersona,
              pTipoId:autorizacion.pTipoId,
              pAutoriza:autorizacion.pAutoriza,
              pCuenta:autorizacion.pCuenta,
              pNormaId:autorizacion.pNormaId,
              pPuntoContactoId:autorizacion.pPuntoContactoId,
              pDescripcion:autorizacion.pDescripcion,
              pDescripcioncontacto:autorizacion.pDescripcioncontacto}

  console.log('objeto resultado',obj);
  return this.http.post(this._url2, obj);


}
eliminarContacto(idContacto: any): Observable<any >{
  console.log("aqui llego a eliminar contacto",idContacto)
  var obj={pId:idContacto}
  return this.http.post(this._url5,obj);
}

}
