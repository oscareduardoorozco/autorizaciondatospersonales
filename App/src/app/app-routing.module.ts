
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutLoginComponent } from './component/aut-login/aut-login.component';
import { AutNormasComponent } from './component/aut-normas/aut-normas.component';
import { AutAutorizacionesComponent } from './component/aut-autorizaciones/aut-autorizaciones.component';
import { AutHomeComponent } from './component/aut-home/aut-home.component';
import { AutContactosComponent } from './component/aut-contactos/aut-contactos.component';
import { NormaListadoComponent } from './component/aut-normas/norma-listado/norma-listado.component';
import { ContactoListadoComponent } from './component/aut-contactos/contacto-listado/contacto-listado.component';
import { AutrizacionesListadoComponent } from './component/aut-autorizaciones/autrizaciones-listado/autrizaciones-listado.component';
import { AutrizacionesContactosComponent } from './component/aut-autorizaciones/autrizaciones-contactos/autrizaciones-contactos.component';
import { AuthGuard } from './guard/auth.guard';
import { ProtectedComponent} from './protected/protected.component';
const routes: Routes = [
  { path: 'login', component: AutLoginComponent },
  { path: 'normas', component: AutNormasComponent },
  { path: 'normaslist', component: NormaListadoComponent },
  { path: 'contactos', component: AutContactosComponent },
  { path: 'contactoslist', component: ContactoListadoComponent },
  { path: 'autorizaciones', component: AutAutorizacionesComponent },
  { path: 'autorizacioneslist', component: AutrizacionesListadoComponent },
  { path: 'autorizacionescont', component: AutrizacionesContactosComponent },
  { path: 'home', component: AutHomeComponent },
  { path: '',   redirectTo: '/login', pathMatch: 'full' },
  { path: 'protected',canActivate:[AuthGuard],component: ProtectedComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
