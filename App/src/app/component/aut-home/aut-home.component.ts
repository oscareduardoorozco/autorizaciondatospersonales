import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

declare var $:any;
@Component({
  selector: 'app-aut-home',
  templateUrl: './aut-home.component.html',
  styleUrls: ['./aut-home.component.css']
})
export class AutHomeComponent implements OnInit {
  showMainContent: Boolean = false;
  acceso: Boolean = false;
  constructor( private router: Router) { }

  ngOnInit(): void {
    this.ShowAcceso();
    $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });
  }

  ShowHideButton() {
    var aValue = localStorage.getItem('perfil');
    console.log(aValue);
    if(aValue =="1"){
      this.showMainContent = true;
      console.log("holaaaaaa home");
    }

   }


   ShowAcceso() {
    var aValue = localStorage.getItem('token');
    console.log(aValue);
    if(aValue ==''){

      this.router.navigate(['login']);
    }
    else{
      this.acceso = true;
   }
  }

   logOut() {
        localStorage.setItem("token",'');
        localStorage.setItem("perfil",'');
        this.router.navigate(['login']);

   }

}
