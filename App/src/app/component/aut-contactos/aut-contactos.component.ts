import { Component,OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TipoContacto } from 'src/app/models/tipocontacto.model';
import { PuntoContactoService } from 'src/app/services/punto-contacto.service';
import {Router} from '@angular/router';
declare var $:any;
@Component({
  selector: 'app-aut-contactos',
  templateUrl: './aut-contactos.component.html',
  styleUrls: ['./aut-contactos.component.css']
})
export class AutContactosComponent implements OnInit {
  showMainContent: Boolean = false;
  acceso: Boolean = false;
  formularitipocontato: any =  //{pCodigo:'123233', pDescripcion:'prueba consumo', pFechaInicio:'14-12-2021', pFechaFin:'14-12-2021'}
  {pNombre:'', pDescripcion:'', pCanal:'', pActivo:''}
 normas: any



 constructor(private toastr: ToastrService,
  private _puntocontactoService: PuntoContactoService,
  private router: Router) {

  }

  ngOnInit(): void {
    this.ShowHideButton();
    this.ShowAcceso();
    $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });

  (function () {
    'use strict'

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event: { preventDefault: () => void; stopPropagation: () => void; }) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }

          form.classList.add('was-validated')

        }, false)
      })
  })()

  }

  agregarContacto(){

    this._puntocontactoService.agregarContacto(this.formularitipocontato).subscribe( resultado => {

      if(Object.values(resultado)[0]==="0"){
        this.toastr.error("faltan datos de registro, verifique por favor", "guardados");
      }
      if(Object.values(resultado)[0]==="1"){
        this.toastr.success("Datos guardados exitosamente", "guardados");
      }
      if(Object.values(resultado)[0]==="2"){
        this.toastr.error("Este punto de contacto ya existe", "guardados");
      }
    });

  }

  //  mostrar y ocultar perfiles
  ShowHideButton() {
    var aValue = localStorage.getItem('perfil');
    debugger
    console.log(aValue);
    if(aValue =="1"){
      this.showMainContent = true;
      console.log("holaaaaaa home");
    }

   }

   ShowAcceso() {
    var token = localStorage.getItem('token');
    var perfil = localStorage.getItem('perfil');
    if(token ==''){
      this.router.navigate(['login']);
    }
    else{
      if(perfil =="2"){
        this.router.navigate(['contactoslist']);

      }
      else{
        this.acceso = true;
      }

   }
  }

   logOut() {
    localStorage.setItem("token",'');
    localStorage.setItem("perfil",'');
    this.router.navigate(['login']);

}

}
