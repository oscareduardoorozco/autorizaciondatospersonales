import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutContactosComponent } from './aut-contactos.component';

describe('AutContactosComponent', () => {
  let component: AutContactosComponent;
  let fixture: ComponentFixture<AutContactosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutContactosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutContactosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
