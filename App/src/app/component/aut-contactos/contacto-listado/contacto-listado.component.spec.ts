import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactoListadoComponent } from './contacto-listado.component';

describe('ContactoListadoComponent', () => {
  let component: ContactoListadoComponent;
  let fixture: ComponentFixture<ContactoListadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContactoListadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactoListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
