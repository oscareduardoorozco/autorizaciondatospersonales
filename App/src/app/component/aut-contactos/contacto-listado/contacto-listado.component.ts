import { HttpClient } from '@angular/common/http';
import { Component, Injectable, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { PuntoContactoService } from 'src/app/services/punto-contacto.service';
import {Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
declare var $:any;
@Component({
  selector: 'app-contacto-listado',
  templateUrl: './contacto-listado.component.html',
  styleUrls: ['./contacto-listado.component.css']
})
export class ContactoListadoComponent implements OnInit {

  formularitipocontato: any =  //{pCodigo:'123233', pDescripcion:'prueba consumo', pFechaInicio:'14-12-2021', pFechaFin:'14-12-2021'}
  {pNombre:'', pDescripcion:'', pCanal:'', pActivo:''}
 normas: any
  showMainContent: Boolean = false;
  acceso: Boolean = false;
  listarpuntocontacto: any[] = [];
  contactoSelecionado :any= {Nombre:'',Descripcion:'',Canal:'',Activo:''};
 eliminarId: any={eliminarContactoId:''}

  constructor(private toastr: ToastrService,
    private http: HttpClient,
    private _puntocontactoService: PuntoContactoService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.ShowAcceso();
    this.ShowHideButton();
    this.listarpuntocontactos();
    $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });

  }


  listarpuntocontactos(){
    this._puntocontactoService.getListarpuntocontacto().subscribe(data => {
      console.log("form",data.Data);
      //var val =data.Data.map(Math.log)
      for (let i in data.Data){
        console.log("form",data.Data[i]);
        this.listarpuntocontacto.push(data.Data[i])
      }
      console.log("for ",this.listarpuntocontacto);
    }, error =>{
      console.log(error);
    });

  }



  // modificar punto de contacto y eliminar modal

  contactoSel(id:any){
    debugger
    for (let i in this.listarpuntocontacto){

      if(id == this.listarpuntocontacto[i].id){
        this.contactoSelecionado={
          Nombre:this.listarpuntocontacto[i].Nombre,
          Descripcion:this.listarpuntocontacto[i].Descripcion,
          Canal:this.listarpuntocontacto[i].Canal,
          Activo:this.listarpuntocontacto[i].Activo
        };
      }
    }
    console.log(this.contactoSelecionado)
  }

  eliminarContacto(nombreEliminar: any){
      console.log("contacto del component",nombreEliminar);
      this._puntocontactoService.eliminarContacto(nombreEliminar).subscribe(data => {
        if(Object.values(data)[0]==="0"){
          this.toastr.error("faltan datos de registro, verifique por favor", "guardados");
        }
        if(Object.values(data)[0]==="1"){
          this.toastr.success("Datos eliminados exitosamente", "guardados");
          this.router.navigate(['autorizacioneslist'])
        }
        if(Object.values(data)[0]==="2"){
          this.toastr.error(" Ha ocurrido un error", "guardados");
        }
      console.log("respuesta del eliminarContacto",data);
      this.router.navigate(['contactoslist'])
      });

  }

  modificarContacto(){

    this._puntocontactoService.modificarContacto(this.contactoSelecionado).subscribe( resultado => {
      if(Object.values(resultado)[0]==="0"){
        $("#modalModificar").modal("hide");
        this.router.navigate(['contactoslist'])
        this.toastr.error("faltan datos de registro, verifique por favor", "guardados");
      }
      if(Object.values(resultado)[0]==="1"){
        $("#modalModificar").modal("hide");
        this.router.navigate(['contactoslist'])
        this.toastr.success("Datos guardados exitosamente", "guardados");
      }
      if(Object.values(resultado)[0]==="2"){
        $("#modalModificar").modal("hide");
        this.router.navigate(['contactoslist'])
        this.toastr.error("Algo fallo, verifique datos", "guardados");
      }});

  }

  ShowHideButton() {
    console.log("holaaaaaa home1");
    var aValue = localStorage.getItem('perfil');
    debugger
    console.log(aValue);
    if(aValue =="1"){
      this.showMainContent = true;
      console.log("holaaaaaa home");
    }

   }

   ShowAcceso() {
    var token = localStorage.getItem('token');
    var perfil = localStorage.getItem('perfil');
    if(token ==''){
      this.router.navigate(['login']);
    }
    else{

        this.acceso = true;
      }
  }

   logOut() {
    localStorage.setItem("token",'');
    localStorage.setItem("perfil",'');
    this.router.navigate(['login']);

}
}
