import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutNormasComponent } from './aut-normas.component';

describe('AutNormasComponent', () => {
  let component: AutNormasComponent;
  let fixture: ComponentFixture<AutNormasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutNormasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutNormasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
