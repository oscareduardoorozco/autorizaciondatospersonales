import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NormaListadoComponent } from './norma-listado.component';

describe('NormaListadoComponent', () => {
  let component: NormaListadoComponent;
  let fixture: ComponentFixture<NormaListadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NormaListadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NormaListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
