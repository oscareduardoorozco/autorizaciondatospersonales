import { HttpClient } from '@angular/common/http';
import { Component, Injectable, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { NormasService } from 'src/app/services/normas.service';
import {Router} from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-norma-listado',
  templateUrl: './norma-listado.component.html',
  styleUrls: ['./norma-listado.component.css'],
})
@Injectable()
export class NormaListadoComponent implements OnInit {
   listarnorma: any[] = [];
   eliminarCodigo: any={eliminarNorma:''}
   showMainContent: Boolean = false;
   acceso: Boolean = false;
   idPersona: any[] = [];

   formularionorma: any = {pCodigo:'', pDescripcion:'', pFechaInicio:'', pFechaFin:''}

   normasSelecionadas :any= {pId:'',pCodigo:'', pDescripcion:'', pFechaInicio:'', pFechaFin:''};

normas: any
alertas: any;
toastr: any;

  constructor(
    private http: HttpClient,
    private _normasService: NormasService,
    private router: Router

  ) {
  }

  ngOnInit(): void {
    this.ShowAcceso();
    this.ShowHideButton();
     this.listarNormas();
    $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
      });
    });


  }

  listarNormas(){
    this._normasService.getListarnormas().subscribe(data => {
      console.log("form",data.Data);
      //var val =data.Data.map(Math.log)
      for (let i in data.Data){
        console.log("form",data.Data[i]);
        this.listarnorma.push(data.Data[i])
      }
      console.log("for ",this.listarnorma);
    }, error =>{
      console.log(error);
    });

  }

  normaSel(id:any){
    for (let i in this.listarnorma){
      debugger
      if(id == this.listarnorma[i].Id){
        this.normasSelecionadas={Id:this.listarnorma[i].pId,
           Codigo:this.listarnorma[i].pCodigo,
          Descripcion:this.listarnorma[i].pDescripcion,
          Fechainicio:this.listarnorma[i].pFechainicio,
          fechaFin:this.listarnorma[i].pfechaFin,
        };
      }
    }
    console.log(this.normasSelecionadas)
  }

  modificarNorma(){
    this._normasService.modificarNorma(this.normasSelecionadas).subscribe( resultado => {
      console.log("resultado del agregar",resultado)
      if(resultado==="Datos de contacto actualizados correctamente"){

        $("#modalModificar").modal("hide");
        this.listarNormas()
        this.toastr.success("Datos guardados exitosamente", "guardados");
        this.router.navigate(['autorizacionescont'])
      }
    },
    error => {
      console.log(JSON.stringify(error));
      this.toastr.error(JSON.stringify(error.error.Message));
    this.toastr.error('No se pueden registrar los datos', 'Error');
    });
  }

  eliminarNorma(CodigoEliminar: any){
    console.log("contacto del component",CodigoEliminar);
    this._normasService.eliminarNorma(CodigoEliminar).subscribe(data => {
    console.log("respuesta del eliminarContacto",data);
    this.listarNormas()
    this.router.navigate(['normaslist'])
    }, error =>{
      console.log(JSON.stringify(error));
    });

}

  ShowHideButton() {
    var aValue = localStorage.getItem('perfil');
    console.log(aValue);
    if(aValue =="1"){
      this.showMainContent = true;
      console.log("holaaaaaa normalist");
    }

   }

   ShowAcceso() {
    var token = localStorage.getItem('token');
    var perfil = localStorage.getItem('perfil');
    if(token ==''){
      this.router.navigate(['login']);
    }
    else{

        this.acceso = true;
      }
  }

   logOut() {
    localStorage.setItem("token",'');
    localStorage.setItem("perfil",'');
    this.router.navigate(['login']);

}

model: any;
model1: any;
}
