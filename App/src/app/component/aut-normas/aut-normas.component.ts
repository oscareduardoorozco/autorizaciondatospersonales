import { Component,OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Norma } from 'src/app/models/norma.model';
import { NormasService } from 'src/app/services/normas.service';
import {Router} from '@angular/router';

declare var $:any;

@Component({
  selector: 'app-aut-normas',
  templateUrl: './aut-normas.component.html',
  styleUrls: ['./aut-normas.component.css']
})
export class AutNormasComponent implements OnInit{

  showMainContent: Boolean = false;
  acceso: Boolean = false;


  formularionorma: any =  //{pCodigo:'123233', pDescripcion:'prueba consumo', pFechaInicio:'14-12-2021', pFechaFin:'14-12-2021'}
     {pCodigo:'', pDescripcion:'', pFechaInicio:'', pFechaFin:''}
    normas: any
  alertas: any;




  constructor( private toastr: ToastrService,
    private _normasServices: NormasService,private router: Router) {
          //  this.form = this.fb.group({
          //   pCodigo: ['', Validators.required],
          //   pDescripcion: ['', Validators.required],
          //   pFechaInicio: ['',Validators.required],
          //   pFechaFin: ['',Validators.required],


          //  })
  }

  ngOnInit(): void {

    this.ShowAcceso();
  this.ShowHideButton();
    $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });
  }

  ShowHideButton() {
  var aValue = localStorage.getItem('perfil');
  console.log(aValue);
  if(aValue =="1"){
    this.showMainContent = true;

  }

 }

 ShowAcceso() {
  var token = localStorage.getItem('token');
  var perfil = localStorage.getItem('perfil');
  if(token ==''){
    this.router.navigate(['login']);
  }
  else{
    if(perfil =="2"){
      this.router.navigate(['normaslist']);

    }
    else{
      this.acceso = true;
    }

 }
}

agregarnorma(){
  this._normasServices.agregarNorma(this.formularionorma).subscribe( resultado => {
   debugger
    console.log("respuesta",Object.values(resultado)[0]);
    if(Object.values(resultado)[0]==="0"){
      this.toastr.error("faltan datos de registro, verifique por favor", "guardados");
    }
    if(Object.values(resultado)[0]==="1"){
      this.toastr.success("Datos guardados exitosamente", "guardados");
    }
    if(Object.values(resultado)[0]==="2"){
      this.toastr.error("Esta norma ya existe", "guardados");
    }

  });

}

logOut() {
  localStorage.setItem("token",'');
  localStorage.setItem("perfil",'');
  this.router.navigate(['login']);

}








  model: any;
  model1: any;



}

