import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutLoginComponent } from './aut-login.component';

describe('AutLoginComponent', () => {
  let component: AutLoginComponent;
  let fixture: ComponentFixture<AutLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
