import { Component} from '@angular/core';
import { FormsModule } from '@angular/forms';
import {FormGroup,FormControl, Validators} from '@angular/forms'
import {LoginService} from '../../services/login.service';
import { LoginI} from '../../models/login.interface';
import { ResponseI} from '../../models/response.interface';
import { ToastrService } from 'ngx-toastr';
import { AlertasService } from '../../services/alertas/alertas.service';
import { NgForm } from '@angular/forms';


import {Router} from '@angular/router';

@Component({
  selector: 'app-aut-login',
  templateUrl: './aut-login.component.html',
  styleUrls: ['./aut-login.component.css']
})
export class AutLoginComponent {
  // email!: string;
  // password!: string;
  loginForm = new FormGroup({
      pUsuario : new FormControl('',Validators.required),
      pContrasena : new FormControl('',Validators.required)
  })



  constructor(private _loginservice: LoginService, private router: Router, private alertas: AlertasService) { }
  ngOnInit(): void {
    (function () {
      'use strict'

      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.querySelectorAll('.needs-validation')

      // Loop over them and prevent submission
      Array.prototype.slice.call(forms)
        .forEach(function (form) {
          form.addEventListener('submit', function (event: { preventDefault: () => void; stopPropagation: () => void; }) {
            if (!form.checkValidity()) {
              event.preventDefault()
              event.stopPropagation()
            }

            form.classList.add('was-validated')
          }, false)
        })
    })()


    }
 onLogin(form:LoginI){

this._loginservice.loginByUsername(form).subscribe(data =>{
     let dataResponse = data;
     debugger
     if(dataResponse != "Errr de credenciales"&& dataResponse.token!=''){
        localStorage.setItem("token",dataResponse.token);
        localStorage.setItem("perfil",dataResponse.perfil);
        this.router.navigate(['home']);

    }
    else{

     this.alertas.showError('Usuario o contraseña incorrectos', 'Error');

    }


})


 }




  }


