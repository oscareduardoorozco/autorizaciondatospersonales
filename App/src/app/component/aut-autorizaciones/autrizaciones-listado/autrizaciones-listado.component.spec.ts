import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutrizacionesListadoComponent } from './autrizaciones-listado.component';

describe('AutrizacionesListadoComponent', () => {
  let component: AutrizacionesListadoComponent;
  let fixture: ComponentFixture<AutrizacionesListadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutrizacionesListadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutrizacionesListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
