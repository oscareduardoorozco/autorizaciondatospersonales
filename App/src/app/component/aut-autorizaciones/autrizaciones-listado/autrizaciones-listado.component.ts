import { HttpClient } from '@angular/common/http';
import { Component, Injectable, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { AutorizacionesService } from 'src/app/services/autorizaciones.service';
import {Router} from '@angular/router';

declare var $:any;
@Component({
  selector: 'app-autrizaciones-listado',
  templateUrl: './autrizaciones-listado.component.html',
  styleUrls: ['./autrizaciones-listado.component.css']
})
export class AutrizacionesListadoComponent implements OnInit {
  showMainContent: Boolean = false;
  acceso: Boolean = false;
  listarautorizaciones: any[] = [];
  listarautorizacionesxId: any[] = [];
  buscarId: any={pPersonaId:''}
  constructor(private http: HttpClient,
    private _autorizacionesService: AutorizacionesService,
    private router: Router
    ) {
     }

  ngOnInit(): void {
    this.ShowAcceso();
    this.ShowHideButton();
    this.listarautorizacion();
    $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });

  }

  listarautorizacion(){
    this._autorizacionesService.getListarautorizacion().subscribe(data => {
      console.log("form de autorizaion",data.Data);
     //la razon de estos ifsasos fue culpa de chiquilin
      for (let i in data.Data){
        if(data.Data[i].TipoId=="1"){
          data.Data[i].TipoId="CC"
        }
        if(data.Data[i].TipoId=="2"){
          data.Data[i].TipoId="NIT"
        }
        if(data.Data[i].TipoId=="3"){
          data.Data[i].TipoId="REPRESENTATE LEGAL"
        }

        if(data.Data[i].Autoriza){
          data.Data[i].Autoriza="si"
        }else{
          data.Data[i].Autoriza="no"
        }
        console.log("form",data.Data[i]);
        this.listarautorizaciones.push(data.Data[i])
      }
      console.log("for ",this.listarautorizaciones);
    }, error =>{
      console.log(error);
    });

  }

  buscarAutorizacionxid(){
    this._autorizacionesService.getListarautorizacionxId(this.buscarId).subscribe(data => {
      debugger

      console.log("form de getListarautorizacionxId",data.Data);
      if(data.Data.length>0){
        this.listarautorizaciones= [];
        for (let i in data.Data){
          if(data.Data[i].TipoId=="1"){
            data.Data[i].TipoId="CC"
          }
          if(data.Data[i].TipoId=="2"){
            data.Data[i].TipoId="NIT"
          }
          if(data.Data[i].TipoId=="3"){
            data.Data[i].TipoId="REPRESENTATE LEGAL"
          }

          if(data.Data[i].Autoriza){
            data.Data[i].Autoriza="si"
          }else{
            data.Data[i].Autoriza="no"
          }
          this.listarautorizaciones.push(data.Data[i])
        }
      }

      console.log("this.listarautorizaciones por id ",this.listarautorizaciones);
    }, error =>{
      console.log(error);
    });
  }

  ShowHideButton() {
    var aValue = localStorage.getItem('perfil');
    debugger
    console.log(aValue);
    if(aValue =="1"){
      this.showMainContent = true;
      console.log("holaaaaaa home");
    }

   }

   ShowAcceso() {
    var token = localStorage.getItem('token');
    var perfil = localStorage.getItem('perfil');
    if(token ==''){
      this.router.navigate(['login']);
    }
    else{

        this.acceso = true;
      }
  }

   logOut() {
    localStorage.setItem("token",'');
    localStorage.setItem("perfil",'');
    this.router.navigate(['login']);

}

}
