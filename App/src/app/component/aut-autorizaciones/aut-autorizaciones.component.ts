import { Component,OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AutorizacionesService } from 'src/app/services/autorizaciones.service';
import { NormasService } from 'src/app/services/normas.service';
import { PuntoContactoService } from 'src/app/services/punto-contacto.service';
import{Router}from '@angular/router';
declare var $:any;
@Component({
  selector: 'app-aut-autorizaciones',
  templateUrl: './aut-autorizaciones.component.html',
  styleUrls: ['./aut-autorizaciones.component.css']
})
export class AutAutorizacionesComponent implements OnInit {
  showMainContent: Boolean = false;
  acceso: Boolean = false;
  listarnorma: any[] = [];
  listarnormaactiva: any[] = [];
  listarpuntocontacto: any[] = [];

  formularioautorizacion: any =
  {pNumero:'',
  pNombrePersona:'',
  pTipoId:'',
  pAutoriza:'',
  pCuenta:'',
  pNormaId:'',
  pPuntoContactoId:'',
  pDescripcion:'',
  pDescripcioncontacto:'',
  pCanal:''}
 autorizacion: any


  constructor(private toastr: ToastrService,
    private _autorizacionService: AutorizacionesService,
    private _normasService: NormasService,
    private _puntocontactoService: PuntoContactoService,
    private router:Router) { }

  ngOnInit(): void {
    this.listarNormasActivas();
    this.ShowAcceso();
    this.ShowHideButton();
    $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });

  (function () {
    'use strict'

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event: { preventDefault: () => void; stopPropagation: () => void; }) {
          if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
          }

          form.classList.add('was-validated')
        }, false)
      })
  })()

  }


  listarNormasActivas(){
    this._normasService.getListarnormasactivas().subscribe(data => {
debugger
      console.log("data de normas",data.Data);
      //var val =data.Data.map(Math.log)
      for (let i in data.Data){
        console.log("form",data.Data[i]);
        this.listarnormaactiva.push(data.Data[i])
      }
      console.log("this.listarnorma ",this.listarnormaactiva);
    }, error =>{
      console.log(error);
    });


    this._puntocontactoService.getListarpuntocontacto().subscribe(data => {
      console.log("data de puntos de contactos",data.Data);
      //var val =data.Data.map(Math.log)
      for (let i in data.Data){
        console.log("form",data.Data[i]);
        this.listarpuntocontacto.push(data.Data[i])
      }
      console.log("this.listarpuntocontacto ",this.listarpuntocontacto);
    }, error =>{
      console.log(error);
    });

  }


  agregarautorizacion(){
    this._autorizacionService.agregarautorizacion(this.formularioautorizacion).subscribe( resultado => {

      if(Object.values(resultado)[0]==="0"){
        this.toastr.error("faltan datos de registro, verifique por favor", "guardados");
      }
      if(Object.values(resultado)[0]==="1"){
        this.toastr.success("Datos guardados exitosamente", "guardados");
        this.router.navigate(['autorizacioneslist'])
      }
      if(Object.values(resultado)[0]==="2"){
        this.toastr.error("Esta persona ya esta asociada a la norma seleccionada", "guardados");
      }
    });

  }
  ShowHideButton() {
    var aValue = localStorage.getItem('perfil');

    console.log(aValue);
    if(aValue =="1"){
      this.showMainContent = true;
      console.log("holaaaaaa home");
    }

   }

   ShowAcceso() {
    var token = localStorage.getItem('token');
    var perfil = localStorage.getItem('perfil');
    if(token ==''){
      this.router.navigate(['login']);
    }
    else{
      if(perfil =="2"){
        this.router.navigate(['autorizacioneslist']);

      }
      else{
        this.acceso = true;
      }

   }
  }

   logOut() {
    localStorage.setItem("token",'');
    localStorage.setItem("perfil",'');
    this.router.navigate(['login']);

}

}
