import { HttpClient } from '@angular/common/http';
import { Component, Injectable, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { AutorizacionesService } from 'src/app/services/autorizaciones.service';
import {Router} from '@angular/router';
declare var $:any;
@Component({
  selector: 'app-autrizaciones-contactos',
  templateUrl: './autrizaciones-contactos.component.html',
  styleUrls: ['./autrizaciones-contactos.component.css']
})
export class AutrizacionesContactosComponent implements OnInit {
  showMainContent: Boolean = false;
  acceso: Boolean = false;
  listarcontacto: any[] = [];
  usuario: any[] = [];
  idPersona: any[] = [];
  showTable:boolean=false;
  showPersona:boolean=false;

  personaSelecionada :any= {Id:'',Contacto:'',Activo:'',TipoContactoId:''};

  formularioautcontacto: any = {pTipoContactoId:'', pContacto:'', pActivo:''}
  buscarId: any={pPersonaId:''}
  eliminarId: any={eliminarContactoId:''}


  constructor(private toastr: ToastrService,
    private http: HttpClient,
    private _autorizacionesService: AutorizacionesService,
    private router:Router
  ) {
  }

  ngOnInit(): void {
    this.ShowAcceso();
    this.ShowHideButton();
    //this.listarcontactos();
    $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
      });
  });

  }

  listarcontactos(){
    console.log("contacto del component",this.buscarId);
    this._autorizacionesService.getListarcontactoautorizacion(this.buscarId).subscribe(data => {
     console.log("respuesta del service listar contacto",data);

     if(data.IsOk=="1"){
      this.listarcontacto= [];
       for (let i in data.Data){
          this.usuario=data.Data[i].NombrePersona
          this.idPersona=data.Data[i].PersonaId
          this.listarcontacto.push(data.Data[i])
        }
        this.showTable=true
        this.showPersona=true
     }else if(data.IsOk=="2"){
      this.listarcontacto= [];
      this.showTable=false
      this.showPersona=true

        for (let i in data.Data){
          this.idPersona=data.Data[i].Id
          this.usuario=data.Data[i].NombreCompleto
        }

        alert("No se encontraron contactos del cliente")
     }else if(data.IsOk=="3"){
        this.listarcontacto= [];
        this.idPersona=[]
        this.usuario= []
        this.showPersona=false
        this.showTable=false
        alert("El cliente no existe")
     }
    }, error =>{
      console.log(error);
    });

  }

  personaSel(id:any){
    for (let i in this.listarcontacto){
      debugger
      if(id == this.listarcontacto[i].Id){
        this.personaSelecionada={Id:this.listarcontacto[i].Id,
          Contacto:this.listarcontacto[i].Contacto,
          Activo:this.listarcontacto[i].Activo,
          TipoContactoId:this.listarcontacto[i].TipoContactoId
        };
      }
    }
    console.log(this.personaSelecionada)
  }

  eliminarContacto(idEliminar: any){
      console.log("contacto del component",idEliminar);
      this._autorizacionesService.eliminarContacto(idEliminar).subscribe(data => {
        if(Object.values(data)[0]==="0"){
          this.toastr.error("faltan datos de registro, verifique por favor", "guardados");
        }
        if(Object.values(data)[0]==="1"){
          this.toastr.success("Datos eliminados exitosamente", "guardados");
          this.router.navigate(['autorizacioneslist'])
        }
        if(Object.values(data)[0]==="2"){
          this.toastr.error(" Ha ocurrido un error", "guardados");
        }
      console.log("respuesta del eliminarContacto",data);
      this.listarcontactos()
      this.router.navigate(['autorizacionescont'])
      });

  }

  agregarautContacto(){

    console.log('aqui entro oscar', this.formularioautcontacto,this.idPersona)
    this._autorizacionesService.agregarautoContacto(this.formularioautcontacto,this.idPersona).subscribe( resultado => {
      console.log("resultado del agregar",resultado)

      if(Object.values(resultado)[0]==="0"){
        $("#modalAgregar").modal("hide");
      this.listarcontactos()
      this.toastr.error("faltan datos de registro, verifique por favor", "guardados");
      this.router.navigate(['autorizacionescont'])

      }
      if(Object.values(resultado)[0]==="1"){
        $("#modalAgregar").modal("hide");
        this.listarcontactos()
        this.toastr.success("Datos guardados exitosamente", "guardados");
        this.router.navigate(['autorizacionescont'])
      }
      if(Object.values(resultado)[0]==="2"){
        $("#modalAgregar").modal("hide");
      this.listarcontactos()
      this.toastr.error("Algo fallo, verifique datos", "guardados");
      this.router.navigate(['autorizacionescont'])

      }



      });

  }

  modificarAutContacto(){
    this._autorizacionesService.modificarAutoContacto(this.personaSelecionada).subscribe( resultado => {
      if(Object.values(resultado)[0]==="0"){
        $("#modalModificar").modal("hide");
        this.router.navigate(['autorizacionescont'])
        this.toastr.error("faltan datos de registro, verifique por favor", "guardados");
      }
      if(Object.values(resultado)[0]==="1"){
        $("#modalModificar").modal("hide");
        this.router.navigate(['autorizacionescont'])
        this.toastr.success("Datos guardados exitosamente", "guardados");
      }
      if(Object.values(resultado)[0]==="2"){
        $("#modalModificar").modal("hide");
        this.router.navigate(['autorizacionescont'])
        this.toastr.error("Algo fallo, verifique datos", "guardados");
      }});
      this.listarcontactos()
  }



  ShowHideButton() {
    var aValue = localStorage.getItem('perfil');

    console.log(aValue);
    if(aValue =="1"){
      this.showMainContent = true;
      console.log("holaaaaaa home");
    }

   }

   ShowAcceso() {
    var token = localStorage.getItem('token');
    var perfil = localStorage.getItem('perfil');
    if(token ==''){
      this.router.navigate(['login']);
    }
  }

   logOut() {
    localStorage.setItem("token",'');
    localStorage.setItem("perfil",'');
    this.router.navigate(['login']);

}

}
