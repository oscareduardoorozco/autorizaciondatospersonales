import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutrizacionesContactosComponent } from './autrizaciones-contactos.component';

describe('AutrizacionesContactosComponent', () => {
  let component: AutrizacionesContactosComponent;
  let fixture: ComponentFixture<AutrizacionesContactosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutrizacionesContactosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutrizacionesContactosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
