import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutAutorizacionesComponent } from './aut-autorizaciones.component';

describe('AutAutorizacionesComponent', () => {
  let component: AutAutorizacionesComponent;
  let fixture: ComponentFixture<AutAutorizacionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutAutorizacionesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutAutorizacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
