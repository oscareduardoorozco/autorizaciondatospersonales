import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AutNormasComponent } from './component/aut-normas/aut-normas.component';
import { AutAutorizacionesComponent } from './component/aut-autorizaciones/aut-autorizaciones.component';
import { AutLoginComponent } from './component/aut-login/aut-login.component';
import { RouterModule, Routes } from '@angular/router';
import { AutHomeComponent } from './component/aut-home/aut-home.component';
import { AutContactosComponent } from './component/aut-contactos/aut-contactos.component';
// import { NormaFormaComponent } from './component/aut-normas/norma-forma/norma-forma.component';
import { NormaListadoComponent } from './component/aut-normas/norma-listado/norma-listado.component';
import { ContactoListadoComponent } from './component/aut-contactos/contacto-listado/contacto-listado.component';
// import { ContactoFormaComponent } from './component/aut-contactos/contacto-forma/contacto-forma.component';
import { AutrizacionesListadoComponent } from './component/aut-autorizaciones/autrizaciones-listado/autrizaciones-listado.component';
// import { AutrizacionesFormaComponent } from './component/aut-autorizaciones/autrizaciones-forma/autrizaciones-forma.component';
import { AutrizacionesContactosComponent } from './component/aut-autorizaciones/autrizaciones-contactos/autrizaciones-contactos.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule}  from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { ToastrModule} from 'ngx-toastr';
import { HttpClientModule} from '@angular/common/http';
import { NormasService } from './services/normas.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CommonModule } from '@angular/common';
import { ErrorTailorModule} from '@ngneat/error-tailor';
import { LocationStrategy, HashLocationStrategy } from "@angular/common";
import { CookieService } from 'ngx-cookie-service';
import { ProtectedComponent } from './protected/protected.component';


const routes: Routes =[
  {path:'normas',component:AutNormasComponent}

];

@NgModule({
  declarations: [
    AppComponent,
    AutNormasComponent,
    AutAutorizacionesComponent,
    AutLoginComponent,
    AutHomeComponent,
    AutContactosComponent,
    NormaListadoComponent,
    ContactoListadoComponent,
    AutrizacionesListadoComponent,
    AutrizacionesContactosComponent,
    ProtectedComponent


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    FormsModule,
    NgbModule,
    MatDatepickerModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    NgxSpinnerModule,
    ErrorTailorModule.forRoot({
      errors: {
        useValue: {
          required: 'Este campo es requerido',
          minlength: ({ requiredLength, actualLength }) =>
                      `Expect ${requiredLength} but got ${actualLength}`,
          invalidAddress: error => `Address isn't valid`
        }
      }
    })



  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },CookieService
  ],
  bootstrap: [AppComponent],



})
export class AppModule { }
